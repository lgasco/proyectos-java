package problem;

import representation.Node;

public interface Problem {
	public boolean isSolution(Node nodo);
	public Node getInicial();
}
